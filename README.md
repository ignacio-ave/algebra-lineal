
```

█ ▄▄ ▀▄    ▄ █▀▄▀█ ██     ▄▄▄▄▀ ▄  █ 
█   █  █  █  █ █ █ █ █ ▀▀▀ █   █   █ 
█▀▀▀    ▀█   █ ▄ █ █▄▄█    █   ██▀▀█ 
█       █    █   █ █  █   █    █   █ 
 █    ▄▀        █     █  ▀        █  
  ▀            ▀     █           ▀   
                    ▀                
```

## Descripción del Proyecto

Este proyecto consiste en la implementación de varias funciones matemáticas utilizando Python. Las funciones están divididas en distintas categorías: Algebra, Estadística, Algebra Lineal, Calculo Diferencial e Integral, y Geometría (Trigonometria actualmente). 

Cada función incluye una breve explicación matemática y está escrita con el objetivo de ser clara y educativa. El código está diseñado para ser fácil de entender y modificar, con comentarios detallados sobre cada paso de los cálculos.


## Librerias usadas

Numpy: Estructura de datos


## Funciones

| Categoría            | Funciones                                                                                                         |
|----------------------|-------------------------------------------------------------------------------------------------------------------|
| **Álgebra**          | `sum_alt`, `sqrt`, `factorial`, `gamma`, `logaritmo`                                                              |
| **Trigonometría**    | `seno`, `coseno`, `tangente`, `angulo_a_radianes`, `radianes_a_angulo`                                            |
| **Estadística**      | `media`, `mediana`, `varianza`, `desviacion_estandar`, `covarianza`, `correlacion`, `covarianza_matriz`, `correlacion_matriz`|
| **Álgebra Lineal**   | `suma_vec`, `resta_vec`, `crear_matriz_ceros`, `crear_matriz_identidad`, `suma_matrices`, `resta_matrices`, `producto_matrices`, `multiplicacion_escalar`, `matriz_transpuesta`, `diagonal_matriz`, `inversa_matriz`, `determinante_matriz`, `matriz_adjunta`, `norma_vector`, `producto_punto`, `producto_cruz`|
| **Cálculo**          | `limite`, `derivada`, `integral`, `eval_legendre_and_deriv`, `newton_raphson_pol_legendre`, `calcular_raices_pesos_legendre`, `cuadratura_gaussiana`|


