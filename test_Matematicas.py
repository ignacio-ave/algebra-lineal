
# Código creado por: Ignacio A.

# test_Matematicas.py

# Este script está diseñado para realizar pruebas automatizadas sobre las funciones definidas en el módulo matematicas.py.
# Los tests se enfocan en validar la corrección de cada función matemática replicando funcionalidades típicas de bibliotecas
# como numpy y math, pero con implementaciones propias que no dependen de estas bibliotecas avanzadas, excepto para comparación.
# Los tests incluyen operaciones con números pequeños y matrices de tamaño 3x3 para garantizar la manejabilidad de los datos.

# Importaciones: numpy se utiliza para la creación y manipulación de arrays y matrices.
# math se emplea para comparar los resultados de las funciones matemáticas propias con sus equivalentes en la biblioteca estándar de Python.
# random y time se utilizan para generar datos de prueba aleatorios y para simular tiempos de espera en las pruebas, respectivamente.
# sys se utiliza para manipulaciones relacionadas con el sistema, como la escritura de salida.

import numpy as np
import math
import random
import time
import sys

from matematicas import (
    sum_alt, sqrt, factorial, gamma, logaritmo, seno, coseno, tangente,
    angulo_a_radianes, radianes_a_angulo, media, mediana, varianza,
    desviacion_estandar, covarianza, std, correlacion, suma_vec, resta_vec,
    crear_matriz_ceros, crear_matriz_identidad, suma_matrices, resta_matrices,
    producto_matrices, multiplicacion_escalar, matriz_transpuesta, 
    diagonal_matriz, inversa_matriz, determinante_matriz, matriz_adjunta,
    norma_vector, producto_punto, producto_cruz, covarianza_matriz,
    correlacion_matriz, limite, derivada, integral, regresion_lineal,
    transposicion_matriz, matriz_cofactores
)

from matematicas import matematica_ascii, PI, E


""" 
   ▄▄▄▄▀ ▄███▄     ▄▄▄▄▄      ▄▄▄▄▀ 
▀▀▀ █    █▀   ▀   █     ▀▄ ▀▀▀ █    
    █    ██▄▄   ▄  ▀▀▀▀▄       █    
   █     █▄   ▄▀ ▀▄▄▄▄▀       █     
  ▀      ▀███▀               ▀      
                                    
"""

"""
def test_suma_alt():
    array = [random.randint(0, 10) for _ in range(10)]
    assert sum_alt(array) == np.sum(array)
    array_2d = np.random.rand(3, 3)
    assert np.allclose(sum_alt(array_2d, eje=0), np.sum(array_2d, axis=0))
"""

def test_sqrt():
    x = random.uniform(0, 100)
    assert abs(sqrt(x) - math.sqrt(x)) < 1e-10

def test_factorial():
    n = random.randint(0, 100)
    assert factorial(n) == math.factorial(n)

def test_gamma():
    n = random.choice([random.randint(1, 10), 0.5])
    try:
        assert abs(gamma(n) - math.gamma(n)) < 1e-10
    except ValueError as e:
        print(f"ValueError for gamma({n}): {e}")

def test_logaritmo():
    x = random.uniform(1, 100)
    base = random.uniform(2, 10)
    assert abs(logaritmo(x, base) - math.log(x, base)) < 1e-10

def test_trigonometricas():
    x = random.uniform(-2*math.pi, 2*math.pi)
    assert abs(seno(x) - math.sin(x)) < 1e-10
    assert abs(coseno(x) - math.cos(x)) < 1e-10
    assert abs(tangente(x) - math.tan(x)) < 1e-10

def test_conversiones_angulos():
    angulo = random.uniform(0, 360)
    radianes = random.uniform(0, 2*math.pi)
    assert abs(angulo_a_radianes(angulo) - math.radians(angulo)) < 1e-10
    assert abs(radianes_a_angulo(radianes) - math.degrees(radianes)) < 1e-10
    
def test_estadisticas():
    data = [random.uniform(0, 100) for _ in range(10)]
    assert abs(media(data) - np.mean(data)) < 1e-10
    assert abs(mediana(data) - np.median(data)) < 1e-10
    assert abs(varianza(data) - np.var(data)) < 1e-10
    assert abs(std(data) - np.std(data, ddof=0)) < 1e-10
    
def test_covarianza_correlacion():
    x = [random.uniform(0, 100) for _ in range(10)]
    y = [random.uniform(0, 100) for _ in range(10)]
    assert abs(covarianza(x, y) - np.cov(x, y, bias=True)[0][1]) < 1e-10
    assert abs(correlacion(x, y) - np.corrcoef(x, y)[0][1]) < 1e-10

def test_operaciones_vectores():
    v = [random.uniform(0, 10) for _ in range(10)]
    w = [random.uniform(0, 10) for _ in range(10)]
    assert np.allclose(suma_vec(v, w), np.add(v, w))
    assert np.allclose(resta_vec(v, w), np.subtract(v, w))
    assert abs(norma_vector(v) - np.linalg.norm(v)) < 1e-10
    assert abs(producto_punto(v, w) - np.dot(v, w)) < 1e-10

def test_matrices():
    A = np.random.rand(3, 3)
    B = np.random.rand(3, 3)
    assert np.allclose(suma_matrices(A, B), np.add(A, B))
    assert np.allclose(resta_matrices(A, B), np.subtract(A, B))
    assert np.allclose(producto_matrices(A, B), np.dot(A, B))
    assert np.allclose(multiplicacion_escalar(A, 2), np.multiply(A, 2))
    assert np.allclose(matriz_transpuesta(A), np.transpose(A))

def test_limites_derivadas():
    f = lambda x: x**2
    x = random.uniform(0.1, 10)
    h = 1e-5
    try:
        limite_derecha = limite(f, x, 'derecha')
        limite_izquierda = limite(f, x, 'izquierda')
        assert abs(limite_derecha - limite_izquierda) < 1e-10
        derivada_val = derivada(f, x, h)
        expected_val = 2 * x
        assert abs(derivada_val - expected_val) < 1e-9  # Aumentar la tolerancia a 1e-9
    except ValueError as e:
        print(f"ValueError for limite or derivada at x={x}: {e}")
    except AssertionError as ae:
        print(f"AssertionError: {ae}, x={x}, h={h}, derivada_val={derivada_val}, expected_val={expected_val}")

def test_regresion():
    x = [random.uniform(0, 10) for _ in range(10)]
    y = [2 * xi + 1 + random.uniform(-1, 1) for xi in x]
    a, b = regresion_lineal(x, y)
    a_np, b_np = np.polyfit(x, y, 1)
    assert abs(a - a_np) < 1e-10
    assert abs(b - b_np) < 1e-10

def test_transposicion_matriz():
    A = np.random.rand(3, 3)
    assert np.allclose(transposicion_matriz(A), np.transpose(A))

def test_diagonal_matriz():
    A = np.random.rand(3, 3)
    assert np.allclose(diagonal_matriz(A), np.diag(A).reshape(-1, 1))

def test_inversa_matriz():
    A = np.random.rand(3, 3)
    while np.linalg.det(A) == 0:  # Ensure matrix is invertible
        A = np.random.rand(3, 3)
    assert np.allclose(inversa_matriz(A), np.linalg.inv(A))

def test_determinante_matriz():
    A = np.random.rand(3, 3)
    assert abs(determinante_matriz(A) - np.linalg.det(A)) < 1e-10

def test_matriz_adjunta():
    A = np.random.rand(3, 3)
    adj = np.linalg.inv(A).T * np.linalg.det(A)
    assert np.allclose(matriz_adjunta(A), adj)

def test_matriz_cofactores():
    A = np.random.rand(3, 3)
    i, j = 1, 1
    cofactores = matriz_cofactores(A, i, j)
    cofactores_esperados = np.delete(np.delete(A, i, axis=0), j, axis=1)
    assert np.allclose(cofactores, cofactores_esperados)

def test_producto_cruz():
    v1 = [random.uniform(0, 10) for _ in range(3)]
    v2 = [random.uniform(0, 10) for _ in range(3)]
    assert np.allclose(producto_cruz(v1, v2), np.cross(v1, v2))

def test_covarianza_matriz():
    matriz = np.random.rand(10, 3)
    calculada = covarianza_matriz(matriz)
    esperada = np.cov(matriz, rowvar=False)
    # print("Calculada:\n", calculada)
    # print("Esperada:\n", esperada)
    assert np.allclose(calculada, esperada)

def test_correlacion_matriz():
    matriz = np.random.rand(10, 3)
    correlacion_calculada = correlacion_matriz(matriz)
    correlacion_esperada = np.corrcoef(matriz, rowvar=False)
    # print("Calculada:\n", correlacion_calculada)
    # print("Esperada:\n", correlacion_esperada)
    assert np.allclose(correlacion_calculada, correlacion_esperada)



""" 
██      ▄▄▄▄▄   ▄█▄    ▄█ ▄█ 
█ █    █     ▀▄ █▀ ▀▄  ██ ██ 
█▄▄█ ▄  ▀▀▀▀▄   █   ▀  ██ ██ 
█  █  ▀▄▄▄▄▀    █▄  ▄▀ ▐█ ▐█ 
   █            ▀███▀   ▐  ▐ 
  █                          
 ▀                           
"""



# Funciones ACII para interfaz de usuario en terminal
def barra_de_carga(tiempo_espera=0.1):
    barra = ["[          ]", "[=         ]", "[==        ]", "[===       ]", "[====      ]", 
             "[=====     ]", "[======    ]", "[=======   ]", "[========  ]", "[========= ]", "[==========]"]
    for i in range(11):
        sys.stdout.write("\r" + barra[i % len(barra)])
        sys.stdout.flush()
        time.sleep(tiempo_espera / 100)
    sys.stdout.write("\r[==========] \u2713 Prueba exitosa\n")
    sys.stdout.flush()

def ejecutar_prueba(funcion_prueba, nombre):
    barra_de_carga()
    try:
        funcion_prueba()
        print(f"{nombre} completada.\n")
    except Exception as e:
        print(f"Error en {nombre}: {e}\n")
# importar variables globales de matematica.py
from Matematicas import matematica_ascii, PI, E


if __name__ == "__main__":
    print(matematica_ascii)  
    print(" Codigo creado por : Ignacio A.")
    print(" Fecha de creacion: 25/05/2024")
    print(" Ultima modificacion: 29/05/2024")
    print(" Version: 0.1")
    
    print("")
    print("")
    
    numero_de_pruebas = int(input("Ingrese el numero de pruebas a realizar: "))
    print("")
    
    pruebas = [
        (test_sqrt, "test_sqrt"),
        (test_factorial, "test_factorial"),
        (test_gamma, "test_gamma"),
        (test_logaritmo, "test_logaritmo"),
        (test_trigonometricas, "test_trigonometricas"),
        (test_conversiones_angulos, "test_conversiones_angulos"),
        (test_estadisticas, "test_estadisticas"),
        (test_covarianza_correlacion, "test_covarianza_correlacion"),
        (test_operaciones_vectores, "test_operaciones_vectores"),
        (test_matrices, "test_matrices"),
        (test_limites_derivadas, "test_limites_derivadas"),
        (test_regresion, "test_regresion"),
        (test_transposicion_matriz, "test_transposicion_matriz"),
        (test_diagonal_matriz, "test_diagonal_matriz"),
        (test_inversa_matriz, "test_inversa_matriz"),
        (test_determinante_matriz, "test_determinante_matriz"),
        (test_matriz_adjunta, "test_matriz_adjunta"),
        (test_matriz_cofactores, "test_matriz_cofactores"),
        (test_producto_cruz, "test_producto_cruz"),
        (test_covarianza_matriz, "test_covarianza_matriz"),
        (test_correlacion_matriz, "test_correlacion_matriz")
    ]
  
      
    for funcion_prueba, nombre in pruebas:
        for i in range(numero_de_pruebas):
            print(f"Iniciando {nombre}...")
            ejecutar_prueba(funcion_prueba, nombre)
        
    print("Todos los tests pasaron exitosamente.")
    