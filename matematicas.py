
# Codigo creado por : Ignacio A.
# Fecha de creacion: 25/05/2024
# Ultima modificacion: 29/05/2024
# Version: 0.2



import numpy as np
import math 
import collections 



# Variables globales 

matematica_ascii = """

█ ▄▄ ▀▄    ▄ █▀▄▀█ ██     ▄▄▄▄▀ ▄  █ 
█   █  █  █  █ █ █ █ █ ▀▀▀ █   █   █ 
█▀▀▀    ▀█   █ ▄ █ █▄▄█    █   ██▀▀█ 
█       █    █   █ █  █   █    █   █ 
 █    ▄▀        █     █  ▀        █  
  ▀            ▀     █           ▀   
                    ▀                
"""

PI = 3.141592653589793
E  = 2.718281828459045

""" 
██   █      ▄▀  ▄███▄   ███   █▄▄▄▄ ██   
█ █  █    ▄▀    █▀   ▀  █  █  █  ▄▀ █ █  
█▄▄█ █    █ ▀▄  ██▄▄    █ ▀ ▄ █▀▀▌  █▄▄█ 
█  █ ███▄ █   █ █▄   ▄▀ █  ▄▀ █  █  █  █ 
   █     ▀ ███  ▀███▀   ███     █      █ 
  █                            ▀      █  
 ▀                                   ▀   
 
"""

#
# Esta función solo se declara. Su propoósito es la comprención teorica de la función sum.
#

def sum_alt(array, eje=None):
    # Args:
    # array (list, np.array): Array de valores.
    # eje (int): Eje a lo largo del cual sumar.
    # Returns:
    # float: Suma de los valores.
    
    # Fundamento Matemático:
    # La suma de un array de valores se define como la suma de todos los elementos del array.
    # Si se especifica un eje, la suma se realiza a lo largo de ese eje.
    
    # Convertir el array a un array de NumPy si no lo es
    if not isinstance(array, np.ndarray):
        array = np.array(array)
    
    # Si no se especifica un eje, sumar todos los elementos
    if eje is None:
        total = 0
        for elemento in array.flat:
            total += elemento
        return total
    
    # Si se especifica un eje, sumar a lo largo de ese eje
    else:
        forma = list(array.shape)
        del forma[eje]
        resultado = np.zeros(forma)
        
        it = np.nditer(array, flags=['multi_index'])
        while not it.finished:
            idx = list(it.multi_index)
            idx[eje] = slice(None)
            index_tuple = tuple(it.multi_index[:eje] + it.multi_index[eje+1:])
            resultado[index_tuple] += array[it.multi_index]
            it.iternext()
        
        return resultado



def sqrt(x, precision=1e-10, max_iter=500):
    # Args:
    # x (float): Número real no negativo.
    # Returns:
    # float: Raíz cuadrada de x.
    
    # Fundamento Matemático:
    # La raíz cuadrada de un número real no negativo x se define como el número real y tal que y^2 = x.
    # También se puede aplicar el método de Newton para encontrar la raíz cuadrada de x.
    
    if x < 0:
        raise ValueError("No se puede calcular la raíz cuadrada de un número negativo")
    
    # Método de Newton para la raíz cuadrada
    r = x # aproximación inicial a resultado
    for _ in range(max_iter):
        r_nuevo = (r + x / r) / 2
        if abs(r - r_nuevo) < precision:
            return r_nuevo
        r = r_nuevo
    
    raise ValueError("No se pudo calcular la raíz cuadrada con la precisión especificada")



def factorial(n):
    # Args:
    # n (int): Número entero no negativo.
    # Returns:
    # int: Factorial de n.
    
    # Fundamento Matemático:
    # El factorial de un número entero no negativo n se define como el producto de todos los enteros
    # positivos menores o iguales a n:
    # n! = n * (n-1) * (n-2) * ... * 2 * 1

    
    if n == 0:
        return 1  # El factorial de 0 es 1 por definición
    resultado = 1
    for i in range(2, n + 1):  # Comienza desde 2 ya que multiplicar por 1 es redundante
        resultado *= i
    return resultado



def gamma(n):
    # Args:
    # n (float): Número real no negativo.
    # Returns:
    # float: Función gamma de n.
    
    # Fundamento Matemático:
    # La función gamma de un número real positivo n se define como:
    # gamma(n) = integral de 0 a infinito de t^(n-1) * e^(-t) dt
    # Para enteros positivos, gamma(n) = (n-1)!
    # Para semi-enteros, gamma(n) = (n-1/2) * (n-3/2) * ... * sqrt(pi)
    
    if n <= 0:
        raise ValueError("El valor debe ser mayor que cero")
    if n - int(n) not in (0, 0.5):
        raise ValueError("Solo se aceptan enteros y semi-enteros")
    
    if n == 0.5:
        return sqrt(PI)
    
    if n < 1:
        return sqrt(PI) / (n * gamma(n + 1))

    resultado = 1
    actual = n - 1
    while actual > 1:
        resultado *= actual
        actual -= 1

    if actual == 0.5:  # Manejar el caso especial de 0.5 factorial.
        resultado *= sqrt(PI)

    return resultado




def logaritmo(x, base=math.e):
    # Args:
    # x (float): Número real positivo.
    # base (float): Base del logaritmo.
    # Returns:
    # float: Logaritmo de x en la base especificada.
    # Raises:
    # ValueError: Si x es menor o igual a 0 o si la base es menor o igual a 0 o 1.
    
    if x <= 0:
        raise ValueError("El número x debe ser positivo")
    if base <= 0 or base == 1:
        raise ValueError("La base debe ser positiva y diferente de 1")
    
    resultado = 0 
    factor = (x-1)/(x+1)
    termino = factor
    n = 1
    # Serie de taylor
    while abs(termino) > 1e-10: # precisión de 10 decimales
        resultado += termino / n
        termino *= factor * factor
        n += 2
        
    resultado *= 2 # ajustar para log(x) desde log((1+y)/(1-y))
    
    if base != math.e:
        resultado /= logaritmo(base)

    return resultado



""" 
   ▄▄▄▄▀ █▄▄▄▄ ▄█   ▄▀  ████▄    ▄   ████▄ █▀▄▀█ ▄███▄     ▄▄▄▄▀ █▄▄▄▄ ██   
▀▀▀ █    █  ▄▀ ██ ▄▀    █   █     █  █   █ █ █ █ █▀   ▀ ▀▀▀ █    █  ▄▀ █ █  
    █    █▀▀▌  ██ █ ▀▄  █   █ ██   █ █   █ █ ▄ █ ██▄▄       █    █▀▀▌  █▄▄█ 
   █     █  █  ▐█ █   █ ▀████ █ █  █ ▀████ █   █ █▄   ▄▀   █     █  █  █  █ 
  ▀        █    ▐  ███        █  █ █          █  ▀███▀    ▀        █      █ 
          ▀                   █   ██         ▀                    ▀      █  
                                                                        ▀   
"""



def seno(x):
    # Args:
    # x (float): Ángulo en radianes.
    # Returns:
    # float: Seno de x.

    # Fundamento Matemático:
    # El seno de un ángulo x se define como la razón entre el cateto opuesto y la hipotenusa
    # en un triángulo rectángulo con ángulo x.
    
    # Reducción de rango para mejorar la precisión
    x = x % (2 * PI)
    resultado = 0
    termino = x
    n = 1
    while abs(termino) > 1e-10:  # precisión de 10 decimales
        resultado += termino
        termino *= - x * x / ((2 * n) * (2 * n + 1))
        n += 1
    return resultado



def coseno(x):
    # Args:
    # x (float): Ángulo en radianes.
    # Returns:
    # float: Coseno de x.

    # Fundamento Matemático:
    # El coseno de un ángulo x se define como la razón entre el cateto adyacente y la hipotenusa
    # en un triángulo rectángulo con ángulo x.

    # Reducción de rango para mejorar la precisión
    x = x % (2 * PI)
    resultado = 0
    termino = 1
    n = 0
    while abs(termino) > 1e-10:  # precisión de 10 decimales
        resultado += termino
        termino *= - x * x / ((2 * n + 1) * (2 * n + 2))
        n += 1
    return resultado



def tangente(x):
    # Args:
    # x (float): Ángulo en radianes.
    # Returns:
    # float: Tangente de x.

    # Fundamento Matemático:
    # La tangente de un ángulo x se define como la razón entre el seno y el coseno de x.

    cos_x = coseno(x)
    if abs(cos_x) < 1e-10:  # Si el coseno es casi 0, la tangente es indefinida.
        return float('inf')
    return seno(x) / cos_x



def angulo_a_radianes(angulo):
    # Args:
    # angulo (float): Ángulo en grados.
    # Returns:
    # float: Ángulo en radianes.
    
    # Fundamento Matemático:
    # La conversión de grados a radianes se realiza multiplicando el ángulo en grados por pi/180.
    
    return angulo * PI / 180



def radianes_a_angulo(radianes):
    # Args:
    # radianes (float): Ángulo en radianes.
    # Returns:
    # float: Ángulo en grados.
    
    # Fundamento Matemático:
    # La conversión de radianes a grados se realiza multiplicando el ángulo en radianes por 180/pi.
    
    return radianes * 180 / PI



"""
▄███▄     ▄▄▄▄▄      ▄▄▄▄▀ ██   ██▄   ▄█    ▄▄▄▄▄      ▄▄▄▄▀ ▄█ ▄█▄    ██   
█▀   ▀   █     ▀▄ ▀▀▀ █    █ █  █  █  ██   █     ▀▄ ▀▀▀ █    ██ █▀ ▀▄  █ █  
██▄▄   ▄  ▀▀▀▀▄       █    █▄▄█ █   █ ██ ▄  ▀▀▀▀▄       █    ██ █   ▀  █▄▄█ 
█▄   ▄▀ ▀▄▄▄▄▀       █     █  █ █  █  ▐█  ▀▄▄▄▄▀       █     ▐█ █▄  ▄▀ █  █ 
▀███▀               ▀         █ ███▀   ▐              ▀       ▐ ▀███▀     █ 
                             █                                           █  
                            ▀                                           ▀   

"""



def media(data):
    # Args:
    # data (list): Lista de valores.
    # Returns:
    # float: Media aritmética.

    # Fundamento Matemático:
    # La media aritmética de un conjunto de n valores x_1, x_2, ..., x_n se define como:
    # media = (x_1 + x_2 + ... + x_n) / n
    # Ejemplo:
    # Si data = [1, 2, 3, 4, 5], entonces:
    # media = (1 + 2 + 3 + 4 + 5) / 5 = 3

    # Verificar que la lista de valores no esté vacía
    if len(data) == 0:
        raise ValueError("La lista de valores está vacía")

    # Calcular la suma de los valores
    suma = sum(data)

    # Calcular la media aritmética
    return suma / len(data)



def mediana(data): 
    # Args:
    # data (list): Lista de valores.
    # Returns:
    # float: Mediana.
    
    # Fundamento Matemático:
    # La mediana de un conjunto de n valores ordenados x_1, x_2, ..., x_n se define como:
    # mediana = x_(n+1)/2 si n es impar
    # mediana = (x_n/2 + x_(n/2 + 1)/2) / 2 si n es par
    # Ejemplo:
    # Si data = [1, 2, 3, 4, 5], entonces:
    # mediana = 3


    sorted_data = sorted(data)
    n = len(data) 
    mid = n // 2
    if n % 2 == 0:
        return (sorted_data[mid - 1] + sorted_data[mid]) / 2
    else:
        return sorted_data[mid]
    


def varianza(data): 
    # Args:
    # data (list): Lista de valores.
    # Returns:
    # float: Varianza.
    
    # Fundamento Matemático:
    # La varianza de un conjunto de n valores x_1, x_2, ..., x_n se define como:
    # varianza = (1/n) * sum((x_i - media)^2) para i = 1, 2, ..., n
    # Ejemplo:
    # Si data = [1, 2, 3, 4, 5], entonces:
    # varianza = (1/5) * ((1-3)^2 + (2-3)^2 + (3-3)^2 + (4-3)^2 + (5-3)^2) = 2
    
    
    m=media(data)
    return sum((x-m)**2 for x in data) / len(data)



def desviacion_estandar(data):
    # Args:
    # data (list): Lista de valores.
    # Returns:
    # float: Desviación estándar.

    # Fundamento Matemático:
    # La desviación estándar de un conjunto de n valores x_1, x_2, ..., x_n se define como:
    # desviacion_estandar = sqrt(varianza)
    # Ejemplo:
    # Si data = [1, 2, 3, 4, 5], entonces:
    # desviacion_estandar = sqrt(2)

    return sqrt(varianza(data))



def covarianza(x,y): 
    # Args:
    # x (list): Lista de valores.
    # y (list): Lista de valores.
    # Returns:
    # float: Covarianza.
    
    # Fundamento Matemático:
    # La covarianza de dos conjuntos de n valores x_1, x_2, ..., x_n e y_1, y_2, ..., y_n se define como:
    # covarianza = (1/n) * sum((x_i - media_x) * (y_i - media_y)) para i = 1, 2, ..., n
    # Ejemplo:
    # Si x = [1, 2, 3, 4, 5] y y = [5, 4, 3, 2, 1], entonces:
    # covarianza = (1/5) * ((1-3)*(5-3) + (2-3)*(4-3) + (3-3)*(3-3) + (4-3)*(2-3) + (5-3)*(1-3)) = -2

    n = len(x)
    media_x = media(x)
    media_y = media(y)
    return sum((x[i] - media_x) * (y[i] - media_y) for i in range(n)) / n



def correlacion(x,y):
    # Args:
    # x (list): Lista de valores.
    # y (list): Lista de valores.
    # Returns:
    # float: Coeficiente de correlación.
    
    # Fundamento Matemático:
    # El coeficiente de correlación de dos conjuntos de n valores x_1, x_2, ..., x_n e y_1, y_2, ..., y_n se define como:
    # correlacion = covarianza / (desviacion_estandar_x * desviacion_estandar_y)
    # Ejemplo:
    # Si x = [1, 2, 3, 4, 5] y y = [5, 4, 3, 2, 1], entonces:
    # correlacion = -2 / (sqrt(2) * sqrt(2)) = -1

    return covarianza(x, y) / (desviacion_estandar(x) * desviacion_estandar(y))



def std(data):
    # Args:
    # data (list): Lista de valores.
    # Returns:
    # float: Desviación estándar.

    # Fundamento Matemático:
    # La desviación estándar de un conjunto de n valores x_1, x_2, ..., x_n se define como:
    # std = sqrt(varianza)
    # Ejemplo:
    # Si data = [1, 2, 3, 4, 5], entonces:
    # varianza = (1/5) * ((1-3)^2 + (2-3)^2 + (3-3)^2 + (4-3)^2 + (5-3)^2) = 2
    # std = sqrt(2)

    return sqrt(varianza(data))


""" 
██   █      ▄▀  ▄███▄   ███   █▄▄▄▄ ██       █    ▄█    ▄   ▄███▄   ██   █     
█ █  █    ▄▀    █▀   ▀  █  █  █  ▄▀ █ █      █    ██     █  █▀   ▀  █ █  █     
█▄▄█ █    █ ▀▄  ██▄▄    █ ▀ ▄ █▀▀▌  █▄▄█     █    ██ ██   █ ██▄▄    █▄▄█ █     
█  █ ███▄ █   █ █▄   ▄▀ █  ▄▀ █  █  █  █     ███▄ ▐█ █ █  █ █▄   ▄▀ █  █ ███▄  
   █     ▀ ███  ▀███▀   ███     █      █         ▀ ▐ █  █ █ ▀███▀      █     ▀ 
  █                            ▀      █              █   ██           █        
 ▀                                   ▀                               ▀         
 
"""



def suma_vec(v, w):
    # Args:
    # v (list): Primer vector.
    # w (list): Segundo vector.
    # Returns:
    # list: Vector suma.

    # Fundamento Matemático:
    # La suma de dos vectores v y w en el espacio R^n se define como el vector u cuyas componentes
    # son la suma de las componentes correspondientes de v y w:
    # u = v + w donde u_i = v_i + w_i para i = 1, 2, ..., n    
    # Ejemplo:
    # Si v = [1, 2] y w = [2, 1], entonces:
    # v + w = [1 + 2, 2 + 1] = [3, 3]

    # Verificar que los vectores tengan la misma longitud
    if len(v) != len(w):
        raise ValueError("Los vectores deben tener la misma longitud")

    # Sumar las componentes correspondientes
    return [v_i + w_i for v_i, w_i in zip(v, w)]



def resta_vec(v, w):
    # Args:
    # v (list): Primer vector.
    # w (list): Segundo vector.
    # Returns:
    # list: Vector diferencia.

    # Fundamento Matemático:
    # La resta de dos vectores v y w en el espacio R^n se define como el vector u cuyas componentes
    # son la diferencia de las componentes correspondientes de v y w:
    # u = v - w donde u_i = v_i - w_i para i = 1, 2, ..., n    
    # Ejemplo:
    # Si v = [1, 2] y w = [2, 1], entonces:
    # v - w = [1 - 2, 2 - 1] = [-1, 1]

    # Verificar que los vectores tengan la misma longitud
    if len(v) != len(w):
        raise ValueError("Los vectores deben tener la misma longitud")

    # Restar las componentes correspondientes
    return [v_i - w_i for v_i, w_i in zip(v, w)]



def sum_alt(arr, ax):
    # Args:
    # arr (np.array): Matriz.
    # ax (int): Eje de la suma.
    # Returns:
    # np.array: Matriz con la suma realizada.
    
    # Fundamento Matemático:
    # La suma de una matriz A en el eje ax se define como una matriz B cuyos elementos son la suma
    # de los elementos de A en el eje ax:
    # B[i, j] = sum(A[i, j, k]) para k = 0, 1, ..., n-1 si ax = 2
    # B[i, j] = sum(A[i, j, k]) para k = 0, 1, ..., m-1 si ax = 0
    # B[i, j] = sum(A[i, j, k]) para k = 0, 1, ..., p-1 si ax = 1

    # necesitamos realizar esto de manera manual
    
    if ax == 0:
        return np.array([np.sum(arr[i, :, :], axis=0) for i in range(arr.shape[0])])
    elif ax == 1:
        return np.array([np.sum(arr[:, i, :], axis=0) for i in range(arr.shape[1])])
    elif ax == 2:
        return np.array([np.sum(arr[:, :, i], axis=0) for i in range(arr.shape[2])])
    else:
        raise ValueError("El eje de la suma debe ser 0, 1 o 2")



def crear_matriz_ceros(m, n):
    # Args:
    # m (int): Número de filas.
    # n (int): Número de columnas.
    # Returns:
    # np.array: Matriz de ceros de tamaño m x n.
    
    return np.zeros((m, n))
   


def crear_matriz_identidad(n):
    # Args:
    # n (int): Tamaño de la matriz.
    # Returns:
    # np.array: Matriz identidad de tamaño n x n.
    
    matriz_identidad = crear_matriz_ceros(n, n)
    for i in range(n):
        matriz_identidad[i, i] = 1
    return matriz_identidad



# Operaciones con matrices


def suma_matrices(A, B):
    # Args:
    # A (np.array): Primera matriz.
    # B (np.array): Segunda matriz.
    # Returns:
    # np.array: Matriz suma.

    assert A.shape == B.shape, "Las dimensiones de las matrices no son compatibles para la suma."
    m, n = A.shape
    S = crear_matriz_ceros(m, n)
    for i in range(m):
        for j in range(n):
            S[i, j] = A[i, j] + B[i, j]
    return S



def resta_matrices(A, B):
    # Args:
    # A (np.array): Primera matriz.
    # B (np.array): Segunda matriz.
    # Returns:
    # np.array: Matriz diferencia.
    
    assert A.shape == B.shape, "Las dimensiones de las matrices no son compatibles para la resta."
    m, n = A.shape
    R = crear_matriz_ceros(m, n)
    for i in range(m):
        for j in range(n):
            R[i, j] = A[i, j] - B[i, j]
    return R



def producto_matrices(A, B):        
    # Args:
    # A (np.array): Primera matriz.
    # B (np.array): Segunda matriz.
    # Returns:
    # np.array: Matriz producto.
    
    assert A.shape[1] == B.shape[0], "Las dimensiones de las matrices no son compatibles para la multiplicación."
    resultado = crear_matriz_ceros(A.shape[0], B.shape[1])
    for i in range(A.shape[0]):
        for j in range(B.shape[1]):
            for k in range(B.shape[0]):
                resultado[i, j] += A[i, k] * B[k, j]
    return resultado



def multiplicacion_escalar(matriz, escalar):
    # Args:
    # matriz (np.array): Matriz.
    # escalar (float): Escalar.
    # Returns:
    # np.array: Matriz multiplicada por el escalar.
    # Fundamento Matemático:
    # La multiplicación de una matriz A por un escalar c se define como una matriz B cuyos elementos son
    # el producto de los elementos de A por c:
    # B[i, j] = c * A[i, j] para i = 1, 2, ..., m y j = 1, 2, ..., n
    
    resultado = crear_matriz_ceros(matriz.shape[0], matriz.shape[1])
    for i in range(matriz.shape[0]):
        for j in range(matriz.shape[1]):
            resultado[i, j] = matriz[i, j] * escalar
    return resultado



def matriz_transpuesta(A):
    # Args:
    # A (np.array): Matriz.
    # Returns:
    # np.array: Matriz transpuesta.
    
    # Fundamento Matemático:
    # La transpuesta de una matriz A se define como una matriz B cuyas filas son las columnas de A:
    # B[i, j] = A[j, i] para i = 1, 2, ..., n y j = 1, 2, ..., m
    
    n, m = A.shape
    transpuesta = crear_matriz_ceros(m, n)
    for i in range(n):
        for j in range(m):
            transpuesta[j, i] = A[i, j]
    return transpuesta



def transposicion_matriz(A):
    # Args:
    # A (np.array): Matriz.
    # Returns:
    # np.array: Matriz transpuesta.
    return matriz_transpuesta(A)



def diagonal_matriz(matriz):
    # Args:
    # matriz (np.array): Matriz cuadrada.
    # Returns:
    # np.array: Vector con la diagonal de la matriz.
    
    # Fundamento Matemático:
    # La diagonal de una matriz cuadrada A se define como un vector cuyos elementos son los elementos
    # de A en la diagonal principal:
    # D[i] = A[i, i] para i = 1, 2, ..., n
    
    assert matriz.shape[0] == matriz.shape[1], "La matriz debe ser cuadrada para extraer la diagonal."
    diagonal = crear_matriz_ceros(matriz.shape[0], 1)
    for i in range(matriz.shape[0]):
        diagonal[i, 0] = matriz[i, i]
    return diagonal



def inversa_matriz(A):
    # Args:
    # A (np.array): Matriz cuadrada.
    # Returns:
    # np.array: Matriz inversa.
    
    # Fundamento Matemático:
    # La inversa de una matriz cuadrada A se define como una matriz B tal que el producto de A por B
    # es la matriz identidad:
    # A * B = I donde I es la matriz identidad
    
    n = A.shape[0]
    A = A.astype(float)
    I = crear_matriz_identidad(n)
    AI = np.hstack((A, I))
    
    for i in range(n):
        if AI[i, i] == 0.0:
            for j in range(i+1, n):
                if AI[j, i] != 0.0:
                    AI[[i, j]] = AI[[j, i]]
                    break
        AI[i] = AI[i] / AI[i, i]
        for j in range(n):
            if i != j:
                AI[j] = AI[j] - AI[j, i] * AI[i]
    
    return AI[:, n:]



def determinante_matriz(A):
    # Args:
    # A (np.array): Matriz cuadrada.
    # Returns:
    # float: Determinante de la matriz.

    # Fundamento Matemático:
    # El determinante de una matriz cuadrada A se define como un número real que se calcula de manera
    # recursiva a partir de los elementos de la matriz:
    # det(A) = sum((-1)^i * A[0, i] * det(A[1:, :i] + A[1:, i+1:]) para i = 0, 1, ..., n-1
    # donde A[1:, :i] + A[1:, i+1:] es la matriz A sin la primera fila y la columna i
    
    
    A = A.astype(float)
    n = A.shape[0]
    det = 1.0
    
    for i in range(n):
        if A[i, i] == 0.0:
            for j in range(i+1, n):
                if A[j, i] != 0.0:
                    A[[i, j]] = A[[j, i]]
                    det *= -1
                    break
        if A[i, i] == 0.0:
            return 0.0
        det *= A[i, i]
        for j in range(i+1, n):
            A[j] = A[j] - A[j, i] / A[i, i] * A[i]
    
    return det



def matriz_adjunta(A):
    # Args:
    # A (np.array): Matriz cuadrada.
    # Returns:
    # np.array: Matriz adjunta.
    
    # Fundamento Matemático:
    # La adjunta de una matriz cuadrada A se define como una matriz B cuyos elementos son los cofactores
    # de A:
    # B[i, j] = (-1)^(i+j) * det(A[1:, :i] + A[1:, i+1:]) para i = 0, 1, ..., n-1 y j = 0, 1, ..., n-1
    # donde A[1:, :i] + A[1:, i+1:] es la matriz A sin la primera fila y la columna i
    
    n, m = A.shape
    adjunta = crear_matriz_ceros(n, m)
    for i in range(n):
        for j in range(m):
            adjunta[i, j] = (-1) ** (i + j) * determinante_matriz(matriz_cofactores(A, i, j))
    return adjunta



def matriz_cofactores(A, i, j):    
    # Args:
    # A (np.array): Matriz cuadrada.
    # i (int): Fila a excluir.
    # j (int): Columna a excluir.
    # Returns:
    # np.array: Matriz de cofactores.

    # Fundamento Matemático:
    # La matriz de cofactores de una matriz cuadrada A se define como una matriz B cuyos elementos son los cofactores
    # de A:
    # B[i, j] = (-1)^(i+j) * det(A[1:, :i] + A[1:, i+1:]) para i = 0, 1, ..., n-1 y j = 0, 1, ..., n-1
    # donde A[1:, :i] + A[1:, i+1:] es la matriz A sin la primera fila y la columna i
    
    n, m = A.shape
    cofactores = crear_matriz_ceros(n-1, m-1)
    for k in range(n):
        if k == i:
            continue
        for l in range(m):
            if l == j:
                continue
            cofactores[k if k < i else k-1, l if l < j else l-1] = A[k, l]
    return cofactores



def norma_vector(v):
    # Args:
    # v (list): Vector.
    # Returns:
    # float: Norma del vector.
    
    # Fundamento Matemático:
    # La norma de un vector v en R^n se define como la raíz cuadrada de la suma de los cuadrados de sus componentes:
    # ||v|| = sqrt(v_1^2 + v_2^2 + ... + v_n^2)
    
    return sum(x ** 2 for x in v) ** 0.5



def producto_punto(v1, v2):
    # Args:
    # v1 (list): Primer vector.
    # v2 (list): Segundo vector.
    # Returns:
    # float: Producto punto.
    
    # Fundamento Matemático:
    # El producto punto de dos vectores v1 y v2 en R^n se define como la suma de los productos de las componentes
    # correspondientes de los vectores:
    # v1 . v2 = v1_1 * v2_1 + v1_2 * v2_2 + ... + v1_n * v2_n

    assert len(v1) == len(v2), "Los vectores deben tener la misma longitud."
    return sum(v1[i] * v2[i] for i in range(len(v1)))



def producto_cruz(v1, v2):
    # Args:
    # v1 (list): Primer vector.
    # v2 (list): Segundo vector.
    # Returns:
    # np.array: Producto cruz.
    
    # Fundamento Matemático:
    # El producto cruz de dos vectores v1 y v2 en R^3 se define como un vector u cuyas componentes son:
    # u_1 = v1_2 * v2_3 - v1_3 * v2_2
    # u_2 = v1_3 * v2_1 - v1_1 * v2_3
    # u_3 = v1_1 * v2_2 - v1_2 * v2_1
    
    assert len(v1) == len(v2) == 3, "Los vectores deben tener longitud 3 para calcular el producto cruz."
    i = v1[1] * v2[2] - v1[2] * v2[1]
    j = v1[2] * v2[0] - v1[0] * v2[2]
    k = v1[0] * v2[1] - v1[1] * v2[0]
    return np.array([i, j, k])



def covarianza_matriz(matriz):
    # Args:
    # matriz (np.array): Matriz de datos.
    # Returns:
    # np.array: Matriz de covarianza.

    # Fundamento Matemático:
    # La covarianza de un conjunto de datos se define como una matriz cuyos elementos son las covarianzas
    # entre las variables del conjunto de datos:
    # cov(X, Y) = sum((X - mean(X)) * (Y - mean(Y))) / (n - 1)

    n, m = matriz.shape
    medias = np.mean(matriz, axis=0)
    covarianza = np.zeros((m, m))

    for i in range(m):
        for j in range(m):
            covarianza[i, j] = np.sum((matriz[:, i] - medias[i]) * (matriz[:, j] - medias[j])) / (n - 1)

    return covarianza



def correlacion_matriz(matriz):
    # Args:
    # matriz (np.array): Matriz de datos.
    # Returns:
    # np.array: Matriz de correlación.

    # Fundamento Matemático:
    # La correlación de un conjunto de datos se define como una matriz cuyos elementos son las correlaciones
    # entre las variables del conjunto de datos:
    # corr(X, Y) = cov(X, Y) / (std(X) * std(Y))
    
    n, m = matriz.shape
    medias = np.mean(matriz, axis=0)
    desviaciones = np.std(matriz, axis=0, ddof=1)
    correlacion = np.zeros((m, m))

    for i in range(m):
        for j in range(m):
            covarianza_ij = np.sum((matriz[:, i] - medias[i]) * (matriz[:, j] - medias[j])) / (n - 1)
            correlacion[i, j] = covarianza_ij / (desviaciones[i] * desviaciones[j])

    return correlacion



def descomposicion_LU(A):
    # Args:
    # A (np.array): Matriz cuadrada.
    # Returns:
    # np.array: Matriz L.
    # np.array: Matriz U.
    
    # Fundamento Matemático:
    # La descomposición LU de una matriz A se define como el producto de una matriz triangular inferior L
    # y una matriz triangular superior U:
    # A = L * U
    
    n = A.shape[0]
    L = crear_matriz_identidad(n)
    U = crear_matriz_ceros(n, n)
    
    for i in range(n):
        for j in range(i, n):
            U[i, j] = A[i, j] - sum(L[i, k] * U[k, j] for k in range(i))
        for j in range(i+1, n):
            L[j, i] = (A[j, i] - sum(L[j, k] * U[k, i] for k in range(i))) / U[i, i]
    
    return L, U



def descomposicion_QR(A):
    # Args:
    # A (np.array): Matriz.
    # Returns:
    # np.array: Matriz Q.
    # np.array: Matriz R.
    
    # Fundamento Matemático:
    # La descomposición QR de una matriz A se define como el producto de una matriz ortogonal Q
    # y una matriz triangular superior R:
    # A = Q * R
    
    A = A.astype(float)  # Asegurarse de que A sea de tipo float
    n, m = A.shape
    Q = np.zeros((n, m))
    R = np.zeros((m, m))
    
    for j in range(m):
        v = A[:, j].copy()
        for i in range(j):
            R[i, j] = np.dot(Q[:, i], A[:, j])  # Utiliza np.dot para el producto punto
            v -= R[i, j] * Q[:, i]
        R[j, j] = norma_vector(v)
        Q[:, j] = v / R[j, j]
    
    return Q, R



"""
▄█▄    ██   █     ▄█▄      ▄   █    ████▄ 
█▀ ▀▄  █ █  █     █▀ ▀▄     █  █    █   █ 
█   ▀  █▄▄█ █     █   ▀  █   █ █    █   █ 
█▄  ▄▀ █  █ ███▄  █▄  ▄▀ █   █ ███▄ ▀████ 
▀███▀     █     ▀ ▀███▀  █▄ ▄█     ▀      
         █                ▀▀▀             
        ▀                                 
        
"""



def limite(f, x, direccion='bidireccional', precision=1e-5):
    # Args:
    # f (callable): Función para la cual se calcula el límite.
    # x (float): Punto hacia el cual se aproxima la variable independiente.
    # direccion (str): Puede ser 'izquierda', 'derecha' o 'bidireccional'.
    # precision (float): Precisión con la que se desea aproximar el límite.
    # Returns:
    # float: Valor aproximado del límite o NaN si los límites no coinciden.
    
    # Fundamento Matemático:
    # El límite de una función f en un punto x se define como el valor al cual se aproxima f cuando la variable
    # independiente se acerca a x. La dirección de aproximación puede ser por la izquierda, por la derecha o
    # bidireccional.
    
    if direccion not in ['izquierda', 'derecha', 'bidireccional']:
        raise ValueError("Dirección debe ser 'izquierda', 'derecha' o 'bidireccional'")

    def aproximacion(delta):
        try:
            return f(x + delta)
        except Exception as e:
            return float('inf')  # Asignar infinito en caso de error en el cálculo

    delta = precision if direccion in ['derecha', 'bidireccional'] else -precision
    limite_derecha = aproximacion(delta)
    limite_izquierda = aproximacion(-delta)

    if direccion == 'bidireccional':
        if abs(limite_derecha - limite_izquierda) < precision:
            return (limite_derecha + limite_izquierda) / 2
        else:
            return float('nan')  # No coincide el límite de ambos lados
    elif direccion == 'derecha':
        return limite_derecha
    else:
        return limite_izquierda
    
    

def derivada(f, x, h=1e-5):
    # Args:
    # f (callable): Función de la cual calcular la derivada.
    # x (float): Punto en el cual calcular la derivada.
    # h (float): Tamaño del paso para el cálculo de la derivada, con un valor predeterminado que balancea precisión y estabilidad.
    # Returns:
    # float: Aproximación de la derivada de f en el punto x.
    
    # Fundamento Matemático:
    # La derivada de una función f en un punto x se define como el límite del cociente incremental de f cuando
    # el tamaño del paso h tiende a cero:
    # f'(x) = lim h -> 0 (f(x + h) - f(x)) / h
    # El método de diferencias finitas es una aproximación numérica de la derivada que utiliza un paso h pequeño.
    
    if h <= 0:
        raise ValueError("El paso h debe ser mayor que cero.")

    try:
        derivada_aproximada = (f(x + h) - f(x - h)) / (2 * h)
    except Exception as e:
        raise ValueError("Error al calcular la derivada: " + str(e))

    return derivada_aproximada



def eval_legendre_and_deriv(n, x):
    # Args:
    # n (int): Grado del polinomio de Legendre.
    # x (float): Punto de evaluación.
    # Returns:
    # tuple: (P_n(x), P'_n(x)) donde P_n es el polinomio de Legendre y P'_n su derivada.
    
    # Fundamento Matemático:
    # El polinomio de Legendre de grado n se define recursivamente a partir de los polinomios de Legendre de grado
    # n-1 y n-2:
    # P_n(x) = ((2n - 1) * x * P_{n-1}(x) - (n - 1) * P_{n-2}(x)) / n
    # La derivada del polinomio de Legendre de grado n se puede calcular a partir de la fórmula de recurrencia:
    # P'_n(x) = n * (x * P_n(x) - P_{n-2}(x)) / (x^2 - 1)
    
    P_n_minus_2 = 1
    P_n_minus_1 = x
    if n == 0:
        return P_n_minus_2, 0
    elif n == 1:
        return P_n_minus_1, 1
    
    for k in range(2, n + 1):
        P_n = ((2 * k - 1) * x * P_n_minus_1 - (k - 1) * P_n_minus_2) / k
        P_n_minus_2 = P_n_minus_1
        P_n_minus_1 = P_n
    
    dP_n = n * (x * P_n - P_n_minus_2) / (x**2 - 1)
    return P_n, dP_n



def newton_raphson_pol_legendre(n, initial_guess, max_iter=100, tolerance=1e-10):
    # Args:
    # n (int): Grado del polinomio de Legendre.
    # initial_guess (float): Aproximación inicial de la raíz.
    # max_iter (int): Número máximo de iteraciones.
    # tolerance (float): Tolerancia para la convergencia.
    # Returns:
    # float: Raíz del polinomio de Legendre de grado n.
    
    # Fundamento Matemático:
    # El método de Newton-Raphson es un algoritmo iterativo para encontrar raíces de una función f.
    # Se basa en la fórmula de recurrencia:
    # x_{n+1} = x_n - f(x_n) / f'(x_n)
    
    x = initial_guess
    for _ in range(max_iter):
        P, dP = eval_legendre_and_deriv(n, x)
        x_new = x - P / dP
        if abs(x - x_new) < tolerance:
            return x_new
        x = x_new
    raise Exception("No se alcanzó la convergencia en el método de Newton-Raphson")



def calcular_raices_pesos_legendre(n):
    # Args:
    # n (int): Grado del polinomio de Legendre.
    # Returns:
    # np.array: Raíces del polinomio de Legendre de grado n.
    # np.array: Pesos de la cuadratura Gaussiana.
    
    # Fundamento Matemático:
    # La cuadratura Gaussiana es un método numérico para aproximar integrales definidas. Se basa en la elección
    # de ciertos nodos (raíces) y pesos para aproximar la integral de una función f en un intervalo [a, b]:
    # int_a^b f(x) dx ≈ sum_i=1^n w_i * f(x_i)
    # Los nodos y pesos para la cuadratura Gaussiana con polinomios de Legendre se pueden calcular a partir de las
    # raíces de los polinomios de Legendre y sus derivadas.
    
    raices = []
    pesos = []
    for i in range(1, n + 1):
        initial_guess = coseno(PI * (i - 0.25) / (n + 0.5))
        raiz = newton_raphson_pol_legendre(n, initial_guess)
        raices.append(raiz)
        _, dP = eval_legendre_and_deriv(n, raiz)
        peso = 2 / ((1 - raiz**2) * (dP**2))
        pesos.append(peso)
    return np.array(raices), np.array(pesos)



def cuadratura_gaussiana(f, a, b, n):
    # Args:
    # f (callable): Función a integrar.
    # a (float): Límite inferior de integración.
    # b (float): Límite superior de integración.
    # n (int): Número de puntos en la cuadratura Gaussiana.
    # Returns:
    # float: Aproximación de la integral de f en [a, b].
    
    # Fundamento Matemático:
    # La cuadratura Gaussiana es un método numérico para aproximar integrales definidas. Se basa en la elección
    # de ciertos nodos (raíces) y pesos para aproximar la integral de una función f en un intervalo [a, b]:
    # int_a^b f(x) dx ≈ sum_i=1^n w_i * f(x_i)
    # Los nodos y pesos para la cuadratura Gaussiana con polinomios de Legendre se pueden calcular a partir de las
    # raíces de los polinomios de Legendre y sus derivadas.
    
    raices, pesos = calcular_raices_pesos_legendre(n)
    h = (b - a) / 2
    c = (b + a) / 2
    integral = sum(w * f(h * x + c) for x, w in zip(raices, pesos))
    return h * integral



def integral(f, a, b, n):
    # Args:
    # f (callable): Función a integrar.
    # a (float): Límite inferior de integración.
    # b (float): Límite superior de integración.
    # n (int): Número de puntos en la cuadratura Gaussiana.
    # Returns:
    # float: Aproximación de la integral de f en [a, b].
    
    return cuadratura_gaussiana(f, a, b, n)



# 
# REGRESION
#

# regresion lineal de manera manual
def normalizar_regresion_lineal(data):
    # Args: 
    # data (list): Lista de valores.
    # Returns:
    # list: Lista de valores normalizados.
    
    # Fundamento Matemático:
    # La normalización de un conjunto de n valores x_1, x_2, ..., x_n se define como:
    # La fórmula de normalización es (x - μ) / σ
    
    # verificar si data es lista de numpy, si no lo es tranformar a lista de numpy
    if not isinstance(data, np.ndarray):
        data = np.array(data)
    
    # Calcular la media y la desviación estándar
    media = media(data)
    desviacion = desviacion_estandar(data)
    data_normalizada = (data - media) / desviacion
    
    return data_normalizada



def regresion_lineal(x, y):
    # Args:
    # x (list): Lista de valores de la variable independiente.
    # y (list): Lista de valores de la variable dependiente.
    # Returns:
    # tuple: Coeficientes de la regresión lineal (a, b).
    
    # Fundamento Matemático:
    # La regresión lineal de dos conjuntos de n valores x_1, x_2, ..., x_n e y_1, y_2, ..., y_n se define como:
    # y = a * x + b
    # donde a = covarianza(x, y) / varianza(x) y b = media(y) - a * media(x)
    
    # verificar si los datos tienen la misma longitud
    if len(x) != len(y):
        raise ValueError("Los vectores deben tener la misma longitud")
    
    # Calcular las medias de x y y
    media_x = media(x)
    media_y = media(y)
    
    # Calcular la covarianza y la varianza
    cov = covarianza(x, y)
    var = varianza(x)
    
    # Calcular los coeficientes de la regresión lineal
    a = cov / var
    b = media_y - a * media_x
    
    return a, b

